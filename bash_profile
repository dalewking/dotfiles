for a in `ls $HOME/.bash_profile.d/*.sh`; do 
  source $a
done

export VISUAL='vi'
export GIT_EDITOR='vim -c start'
export SVN_EDITOR='vim -c start'
export EDITOR=$VISUAL

export ANDROID_HOME="/Users/DALEK/AndroidSDK"
export JAVA_HOME=$(/usr/libexec/java_home)

export PATH=$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools:$PATH

if [[ -s ~/.rvm/scripts/rvm ]] ; then source ~/.rvm/scripts/rvm ; fi

test -r /sw/bin/init.sh && source /sw/bin/init.sh

##
# Your previous /Users/dalewking/.bash_profile file was backed up as /Users/dalewking/.bash_profile.macports-saved_2013-10-09_at_12:11:57
##

# MacPorts Installer addition on 2013-10-09_at_12:11:57: adding an appropriate PATH variable for use with MacPorts.
export PATH=/opt/local/bin:/opt/local/sbin:$PATH
# Finished adapting your PATH environment variable for use with MacPorts.


##
# Your previous /Users/dalewking/.bash_profile file was backed up as /Users/dalewking/.bash_profile.macports-saved_2013-11-20_at_11:20:30
##

# MacPorts Installer addition on 2013-11-20_at_11:20:30: adding an appropriate PATH variable for use with MacPorts.
export PATH=/opt/local/bin:/opt/local/sbin:$PATH
# Finished adapting your PATH environment variable for use with MacPorts.


PATH=$PATH:$HOME/.manymo/bin # Add manymo to PATH for scripting

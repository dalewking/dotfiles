# Make Bash append rather than overwrite the history on disk
shopt -s histappend

# Whenever displaying the prompt, write the previous line to disk
PROMPT_COMMAND='history -a'

alias vim='vim -c start'
alias mvim='mvim -c start'
alias bp='/usr/bin/pico ~/.bash_profile && source ~/.bash_profile'
alias gl='git log'
alias gd='git diff'
alias gco='git checkout'
alias gca='git commit -a'
alias gc='git commit'
alias gbr='git checkout -b'
alias gs='git status'
function gm()
{
  set current-branch "$(git symbolic-ref HEAD)"
  if [ -z "${current-branch}" ]
  then
    echo 'Not on a branch'
    exit 1
  else
    git checkout ${1}
    git merge ${current-branch}
    git checkout ${current-branch}
  fi
}
alias gundo='git rebase --soft HEAD^'
alias gpull='git pull'
alias gpush='git push'
alias gmv='git mv'
alias ga='git add .'
alias gai='git add -i .'
alias dir='ls -la'
alias copy='cp'
alias ls-al='ls -al'   #fix typo missing space
alias ll="ls -l"
alias la="ls -la"
alias cd..='cd ..'     #fix typo missing space
alias ..='cd ..'
alias .='echo $PWD'

alias sgi='sudo gem install'

export CLICOLOR=1
export LSCOLORS=ExFxCxDxBxegedabagacad

export PATH=/opt/local/bin:/opt/local/sbin:$PATH

